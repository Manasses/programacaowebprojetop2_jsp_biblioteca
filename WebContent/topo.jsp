<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<title>Header</title>
<link rel="stylesheet" href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
<link rel="icon" type="image/png" href="/ProgramacaoWebProjetoP2_JSP/img/programacao_web.png" />
</head>
<body>

	<div
		style="width: 100%; height: 150px; border: 0px solid; background-color: #D2DAEC; font-family: Century Gothic, monospace; vertical-align: middle; line-height: 150px; text-align: center; font-size: 80px;">
		Programação Web</div>
	<ul style="font-family: Century Gothic, monospace;">
		<li><a class="active" href="/ProgramacaoWebProjetoP2_JSP/">Home</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/aluno/cadastrarAluno.jsp">Cadastrar	Aluno</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/funcionario/cadastrarFuncionario.jsp">Cadastrar	Funcionário</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/material/cadastrarMaterial.jsp">Cadastro de Material</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/listarAlunos">Listar Alunos</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/listarFuncionarios">Listar Funcionários</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/listarMateriais">Listar Materiais</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/listarLocacoes">Listar Locações</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/listarDevolucoes">Listar Devoluções</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/locar_material/locarMaterial.jsp">Locar Material</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/reservar_material/reservarMaterial.jsp">Reservar Material</a></li>
		<li><a href="/ProgramacaoWebProjetoP2_JSP/pages/devolver_material/devolverMaterial.jsp">Devolver Material</a></li>
		<!--  
			<li style="float: right;"><a href="#about">About</a></li>
		-->
	</ul>

</body>
</html>