<%@ page import="dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Material/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<body>

	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" action="alterarMaterialEfetivacao"
		style="font-family: monospace; font-size: 20px;" method="post">

		<div>
			<center>
				<h1>Alterar Material</h1>
			</center>
		</div>
	
		<% 
		Material material = (Material) request.getAttribute("material"); 
		String dataCadastro = (String) request.getAttribute("dataCadastro");
		%>
		
		<input type="hidden" name="id" value="${material.id}">
		
		<table align="center">
			<tr>
				<td><label>ID: </label></td>
				<td><input type="text" value="${material.id}" disabled="disabled"/></td>
			</tr>
			<tr>
				<td><label>T�tulo do material: </label></td>
				<td><input type="text" name="tituloDoMaterial"  value="${material.titulo}"/></td>
			</tr>

			<tr>
				<td><label>Nome do autor: </label></td>
				<td><input type="text" name="nomeDoAutor"  value="${material.autor}"/></td>
			</tr>
			<tr>
				<td><label>Nome da editora: </label></td>
				<td><input type="text" name="nomeDaEditora"  value="${material.editora}"/></td>
			</tr>
			<tr>
				<td><label>Data de cadastro: </label></td>
				<td><input type="text" value="${material.dataCadastro}" disabled="disabled"/></td>
			</tr>
			<tr>
				<td><label>Tipo de material: </label></td>
				<td><select name="tipoDeMaterial" class="marginLeft20px" value="${material.tipoMaterial}" dir="${material.tipoMaterial}" onfocus="${material.tipoMaterial}" tabindex="${material.tipoMaterial}" title="${material.tipoMaterial}" jsfc="${material.tipoMaterial}">
						<option name="Livro">Livro</option>
						<option name="Revista">Revista</option>
						<option name="Video">V�deo(CD/DVD)</option>
				</select></td>
			</tr>
			<tr>
				<td><label>Quantidade de itens: </label></td>
				<td><input type="text" name="qntDeItens"  value="${material.qntDisponivel}"/></td>
			</tr>
		</table>

		 <div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;" value="Alterar Informa��es do Material"/>
		</div>

	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>