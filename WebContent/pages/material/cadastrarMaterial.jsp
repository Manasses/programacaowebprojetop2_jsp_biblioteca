<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Cadastrar Material</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<script>
	function validacoes() {
		var tituloDoMaterial = document.getElementById("tituloDoMaterial").value;
		var nomeDoAutor = document.getElementById("nomeDoAutor").value;
		var nomeDaEditora = document.getElementById("nomeDaEditora").value;
		var tipoDeMaterial = document.getElementById("tipoDeMaterial").value;
		var qntDeItens = document.getElementById("qntDeItens").value;

		if (tituloDoMaterial === '' || tituloDoMaterial === null) {
			alert("T�tulo n�o preenchido!");
			return false;
		} 
		else if (nomeDoAutor === '' || nomeDoAutor === null) {
			alert("Nome do autor n�o preenchido!");
			return false;
		} 
		else if (nomeDaEditora === '' || nomeDaEditora === null) {
			alert("Nome da editora n�o preenchido!");
			return false;
		} 
		else if (tipoDeMaterial === '' || tipoDeMaterial === null) {
			alert("T�tulo do material n�o preenchido!");
			return false;
		} 
		else if (qntDeItens === '' || qntDeItens === null) {
			alert("Quantidade de itens n�o preenchido!");
			return false;
		} 
		else {
			return true;
		}

	}
</script>

<body>
	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" style="font-family: monospace; font-size: 20px;" action="../../cadastrarMaterial" method="post">

		<div>
			<center>
				<h1>Cadastrar Material</h1>
			</center>
		</div>

		<table align="center">
			<tr>
				<td><label>T�tulo do material: </label></td>
				<td><input type="text" name="tituloDoMaterial" id="tituloDoMaterial"/></td>
			</tr>

			<tr>
				<td><label>Nome do autor: </label></td>
				<td><input type="text" name="nomeDoAutor" id="nomeDoAutor"/></td>
			</tr>
			<tr>
				<td><label>Nome da editora: </label></td>
				<td><input type="text" name="nomeDaEditora" id="nomeDaEditora"/></td>
			</tr>
			<tr>
				<td><label>Tipo de material: </label></td>
				<td><select name="tipoDeMaterial" class="marginLeft20px" id="tipoDeMaterial">
						<option name="Livro">Livro</option>
						<option name="Revista">Revista</option>
						<option name="Video">V�deo(CD/DVD)</option>
				</select></td>
			</tr>
			<tr>
				<td><label>Quantidade de itens: </label></td>
				<td><input type="text" name="qntDeItens" id="qntDeItens"/></td>
			</tr>
		</table>

		<div align="center">
			<input type="submit" onclick="javascript:return validacoes()"
				style="width: 200px; height: 30px; font-family: Century Gothic;" value="Cadastrar Material"/>
		</div>
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>

</body>
</html>