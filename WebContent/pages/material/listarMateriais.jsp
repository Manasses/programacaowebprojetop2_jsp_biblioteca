<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.*"%>
<%@ page import="dao.*"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Material/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>

<body onload="">

	<%@include file="/topo.jsp"%>
	
	<% 
	List<Material> listaMateriais = (List<Material>) request.getAttribute("listaDeMateriais");
	%>

	<c:set var="lista" />
	<%pageContext.setAttribute("lista", listaMateriais); %>
	
	<br />
	
	<div align="center" style="font-family: monospace; font-size: 20px;">
	
	<div>
		<center>
			<h1>Listar Materiais</h1>
		</center>
	</div>
	
		<table border="1">
			<tr>
				<th><c:out value="C�digo" /></th>
				<th><c:out value="T�tulo" /></th>
				<th><c:out value="Autor" /></th>
				<th><c:out value="Editora" /></th>
				<th><c:out value="Data de Cadastro" /></th>
				<th><c:out value="Tipo de Material" /></th>
				<th><c:out value="Quantidade Dispon�vel" /></th>
				<th><c:out value="Quantidade Ocupada" /></th>
				<th><c:out value="Alterar" /></th>
				<th><c:out value="Excluir" /></th>
			</tr>
			<c:forEach items="${lista}" var="material">
				<tr>
					<td><c:out value="${material.id}" /></td>
					<td><c:out value="${material.titulo}" /></td>
					<td><c:out value="${material.autor}" /></td>
					<td><c:out value="${material.editora}" /></td>
					<td><c:out value="${material.dataCadastro}" /></td>
					<td><c:out value="${material.tipoMaterial}" /></td>
					<td><c:out value="${material.qntDisponivel}" /></td>
					<td><c:out value="${material.qntOcupada}" /></td>
					<td><a href="alterarMaterial?id=${material.id}"><c:out value="Alterar" /></a></td>
					<td><a href="excluirMaterial?id=${material.id}" onclick="javascript:return confirm('Deseja Excluir?')"><c:out value="Excluir" /></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>


	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>