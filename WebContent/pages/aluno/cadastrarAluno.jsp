<%@ page import="dao.*"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>

<script>
	function validacoes() {
		var nome = document.getElementById("nome").value;
		var nomeDaMae = document.getElementById("nomedamae").value;
		var cpf = document.getElementById("cpf").value;
		var dataNascimento = document.getElementById("dataNascimento").value;
		var telefone = document.getElementById("telefone").value;

		if (nome === '' || nome === null) {
			alert("Nome n�o preenchido!");
			return false;
		} 
		else if (nomeDaMae === '' || nomeDaMae === null) {
			alert("Nome da m�e n�o preenchido!");
			return false;
		} 
		else if (cpf === '' || cpf === null) {
			alert("CPF n�o preenchido!");
			return false;
		} 
		else if (dataNascimento === '' || dataNascimento === null) {
			alert("Data de Nascimento n�o preenchido!");
			return false;
		} 
		else if (telefone === '' || telefone === null) {
			alert("Telefone n�o preenchido!");
			return false;
		} 
		else {
			return true;
		}

	}
</script>

<body>

	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" action="../../cadastrarAluno"
		style="font-family: monospace; font-size: 20px;" method="post">

		<div>
			<center>
				<h1>Cadastrar Aluno</h1>
			</center>
		</div>

		<table align="center">
			<tr>
				<td><label>Nome: </label></td>
				<td><input type="text" name="nome" id="nome" /></td>
			</tr>

			<tr>
				<td><label>Nome da m�e: </label></td>
				<td><input type="text" name="nomedamae" id="nomedamae"/></td>
			</tr>

			<tr>
				<td><label>CPF: </label></td>
				<td><input type="text" name="cpf"
					onBlur="ValidarCPF(form1.cpf);" onKeyPress="MascaraCPF(form1.cpf);"
					maxlength="14" id="cpf"/></td>
			</tr>
			<tr>
				<td><label>Data de Nascimento: </label></td>
				<td><input type="date" name="dataNascimento" id="dataNascimento"/></td>
			</tr>
			<tr>
				<td><label>Telefone: </label></td>
				<td><input type="text" name="telefone" id="telefone"
					onKeyPress="MascaraTelefone(form1.telefone);" maxlength="14"
					onBlur="ValidaTelefone(form1.tel);" /></td>
			</tr>
		</table>

		<div align="center">
			<input type="submit" onclick="javascript:return validacoes()"
				style="width: 200px; height: 30px; font-family: Century Gothic;"></input>
		</div>
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>