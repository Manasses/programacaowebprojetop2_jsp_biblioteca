<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.*"%>
<%@ page import="dao.*"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>

<body onload="">

	<%@include file="/topo.jsp"%>

	<%
		List<Aluno> listaAlunos = (List<Aluno>) request.getAttribute("listaDeAlunos");
	%>

	<c:set var="lista" />
	<%
		pageContext.setAttribute("lista", listaAlunos);
	%>

	<br />

	<div align="center" style="font-family: monospace; font-size: 20px;">

		<div>
			<center>
				<h1>Listar Alunos</h1>
			</center>
		</div>

	<!-- 	<form name="form1" action="../../alterarAluno"
			style="font-family: monospace; font-size: 20px;" method="post">
 -->
			<table border="1">
				<tr>
					<th><c:out value="Nome" /></th>
					<th><c:out value="Nome da m�e" /></th>
					<th><c:out value="CPF" /></th>
					<th><c:out value="Data de Nascimento" /></th>
					<th><c:out value="Telefone" /></th>
					<th><c:out value="Alterar" /></th>
					<th><c:out value="Excluir" /></th>
				</tr>
				<c:forEach items="${lista}" var="aluno">
					<tr>
						<td><c:out value="${aluno.nome}" /></td>
						<td><c:out value="${aluno.nomeDaMae}" /></td>
						<td><c:out value="${aluno.cpf}" /></td>
						<td><c:out value="${aluno.dataDeNascimento}" /></td>
						<td><c:out value="${aluno.telefone}" /></td>
						<td><a href="alterarAluno?id=${aluno.id}"><c:out value="Alterar" /></a></td>
						<td><a href="excluirAluno?id=${aluno.id}" onclick="javascript:return confirm('Deseja Excluir?')"><c:out value="Excluir" /></a></td>
					</tr>
				</c:forEach>
			</table>
		<!-- </form> -->
	</div>


	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>