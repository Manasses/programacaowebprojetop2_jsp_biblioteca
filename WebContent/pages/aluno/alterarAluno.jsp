<%@ page import="dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<body>

	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" action="alterarAlunoEfetivacao"
		style="font-family: monospace; font-size: 20px;" method="post">

		<div>
			<center>
				<h1>Alterar Aluno</h1>
			</center>
		</div>
	
		<% 
		Aluno aluno = (Aluno) request.getAttribute("aluno"); 
		String dataNascimento = (String) request.getAttribute("dataNascimento");
		%>
		
		<input type="hidden" name="id" value="${aluno.id}">
		
		<table align="center">
			<tr>
				<td><label>ID: </label></td>
				<td><input type="text" value="${aluno.id}" disabled="disabled"/></td>
			</tr>
<tr>
				<td><label>Nome: </label></td>
				<td><input type="text" name="nome" value="${aluno.nome}"/></td>
			</tr>

			<tr>
				<td><label>Nome da m�e: </label></td>
				<td><input type="text" name="nomedamae" value="${aluno.nomeDaMae}"/></td>
			</tr>

			<tr>
				<td><label>CPF: </label></td>
				<td><input type="text" name="cpf"
					onBlur="ValidarCPF(form1.cpf);" onKeyPress="MascaraCPF(form1.cpf);"
					maxlength="14" value="${aluno.cpf}"/></td>
			</tr>
			<tr>
				<td><label>Data de Nascimento: </label></td>
				<td><input type="date" name="dataNascimento" value="${dataNascimento}"/></td>
			</tr>
			<tr>
				<td><label>Telefone: </label></td>
				<td><input type="text" name="telefone"
					onKeyPress="MascaraTelefone(form1.telefone);" maxlength="14"
					onBlur="ValidaTelefone(form1.tel);" value="${aluno.telefone}"/></td>
			</tr>
		</table>

		 <div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;" value="Alterar Informa��es do Aluno"/>
		</div>
		<!-- 
		 <input type="submit" value="Enviar" />
		 -->
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>