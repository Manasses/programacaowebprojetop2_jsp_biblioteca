<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Cadastrar Material</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<body>
	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" style="font-family: monospace; font-size: 20px;">

		<div>
			<center>
				<h1>Reservar Material</h1>
			</center>
		</div>

		<table align="center">
			<tr>
				<td><label>C�digo do material: </label></td>
				<td><input type="text" name="tituloDoMaterial" /></td>
				<td><input type="button" value="Pesquisar Material" /></td>
			</tr>
			<tr>
				<td><label>Data para reservar: </label></td>
				<td><input type="date" name="dataParaReservar" /></td>
			</tr>
			<tr>
				<td><label>Tipo de usu�rio: </label></td>
				<td><select name="tipoDePessoa" class="marginLeft20px">
						<option name="option01">Aluno</option>
						<option name="option02">Funcion�rio</option>
				</select></td>
			</tr>
			<tr>
				<td><label>CPF do usu�rio: </label></td>
				<td><input type="text" name="usuario" /></td>
				<td><input type="button" value="Pesquisar Usu�rio" /></td>
			</tr>

		</table>

		<div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;" value="Reservar Material"/>
		</div>
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>

</body>
</html>