<%@ page import="dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Cadastrar Material</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
<script src="jquery-latest.js">
	
</script>
<script>
	$(document).ready(function() {
		$('#submit').click(function(event) {
			var username = $('#idMaterial').val();
			$.get('../../ConsultarMaterial', {
				idMaterial : username
			}, function(responseText) {
				$('#tituloMaterial').attr("value", responseText);
			});
		});
	});

	$(document).ready(function() {
		$('#submit2').click(function(event) {
			var username = $('#cpfUsuario').val();
			$.get('../../ConsultarUsuario', {
				cpfUsuario : username
			}, function(responseText) {
				$('#nomeUsuario').attr("value", responseText);
			});
		});
	});
</script>
</head>
<body>
	<%@include file="/topo.jsp"%>

	<br />

	<form name="form1" action="../../reservarMaterial"
		style="font-family: monospace; font-size: 20px;" method="post">

		<div>
			<center>
				<h1>Reservar Material</h1>
			</center>
		</div>
		
		<% 
		/*Material material = (Material) request.getAttribute("material"); 
		Aluno aluno = (Aluno) request.getAttribute("aluno"); 
		Funcionario funcionario = (Funcionario) request.getAttribute("funcionario");*/
		String mensagem = (String) request.getAttribute("mensagem");
		%>
		
		<div>
			<c:out value="${mensagem}"></c:out>
		</div>
		
		<div id="welcometext"></div>
		<table align="center">
			<tr>
				<td><label>C�digo do material: </label></td>
				<td><input type="text" name="idMaterial" id="idMaterial" /></td>
			</tr>
			<tr>
				<td><label>Data para reservar: </label></td>
				<td><input type="date" name="dataParaReservar" /></td>
			</tr>
			<tr>
				<td><label>CPF do usu�rio: </label></td>
				<td><input type="text" name="cpf"
					onBlur="ValidarCPF(form1.cpf);" id="cpfUsuario" onKeyPress="MascaraCPF(form1.cpf);"
					maxlength="14" /></td>
			</tr>
		</table>

		<div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;"
				value="Reservar Material" />
		</div>
	
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>

</body>
</html>