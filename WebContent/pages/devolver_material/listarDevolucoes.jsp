<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.*"%>
<%@ page import="dao.*"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>

<body onload="">

	<%@include file="/topo.jsp"%>

	<%
		List<Devolucao> listaDevolucoes = (List<Devolucao>) request.getAttribute("listaDeDevolucoes");
	%>

	<c:set var="lista" />
	<%
		pageContext.setAttribute("lista", listaDevolucoes);
	%>

	<br />

	<div align="center" style="font-family: monospace; font-size: 20px;">

		<div>
			<center>
				<h1>Listar Alunos</h1>
			</center>
		</div>

	<!-- 	<form name="form1" action="../../alterarAluno"
			style="font-family: monospace; font-size: 20px;" method="post">
 -->
			<table border="1">
				<tr>
					<th><c:out value="Aluno" /></th>
					<th><c:out value="Professor" /></th>
					<th><c:out value="Data de devolu��o" /></th>
					<th><c:out value="Atraso na devolu��o" /></th>
					<th><c:out value="Dias de atraso" /></th>
					<th><c:out value="Valor di�rio da multa" /></th>
					<th><c:out value="Valor total da multa" /></th>
					<th><c:out value="Pagamento realizado" /></th>
					<th><c:out value="Pagamento necess�rio" /></th>
				</tr>
				<c:forEach items="${lista}" var="devolucao">
					<tr>
						<td><c:out value="${devolucao.locacao.aluno.nome}" /></td>
						<td><c:out value="${devolucao.locacao.funcionario.nome}" /></td>
						<td><c:out value="${devolucao.dataDevolucao}" /></td>
						<td><c:out value="${devolucao.atrasoDevolucao}" /></td>
						<td><c:out value="${devolucao.qntDiasDeAtraso}" /></td>
						<td><c:out value="${devolucao.valorDiarioDaMulta}" /></td>
						<td><c:out value="${devolucao.valorTotalDaMulta}" /></td>
						<td><c:out value="${devolucao.pagamentoRealizado}" /></td>
						<td><c:out value="${devolucao.pagamentoNecessario}" /></td>
					</tr>
				</c:forEach>
			</table>
		<!-- </form> -->
	</div>


	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>