<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Cadastrar Material</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<body>
	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" action="../../efetuarDevolucao" 
		style="font-family: monospace; font-size: 20px;">

		<div>
			<center>
				<h1>Devolver Material</h1>
			</center>
		</div>

		<table align="center">
			<tr>
				<td><label>C�digo da loca��o: </label></td>
				<td><input type="text" name="codigoLocacao" /></td>
				<td><input type="button" value="Pesquisar Loca��o" /></td>
			</tr>
		</table>

		<div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;"
				value="Efetuar Devolu��o" />
		</div>
	</form>

	<br />

	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>

</body>
</html>