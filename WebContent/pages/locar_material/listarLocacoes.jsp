<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.*"%>
<%@ page import="dao.*"%>
<%@ page import="model.*"%>
<%@ page import="controller.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>

<body onload="">

	<%@include file="/topo.jsp"%>

	<%
		List<Aluno> listaLocacoes = (List<Aluno>) request.getAttribute("listaDeLocacoes");
	%>

	<c:set var="lista" />
	<%
		pageContext.setAttribute("lista", listaLocacoes);
	%>

	<br />

	<div align="center" style="font-family: monospace; font-size: 20px;">

		<div>
			<center>
				<h1>Listar Loca��es</h1>
			</center>
		</div>

	<!-- 	<form name="form1" action="../../alterarAluno"
			style="font-family: monospace; font-size: 20px;" method="post">
 -->
			<table border="1">
				<tr>
					<th><c:out value="C�digo da Loca��o" /></th>
					<th><c:out value="Aluno" /></th>
					<th><c:out value="Funcion�rio" /></th>
					<th><c:out value="T�tulo do material" /></th>
					<th><c:out value="Data de Loca��o" /></th>
					<th><c:out value="Data de Devolu��o" /></th>
					<th><c:out value="Reserva" /></th>
				</tr>
				<c:forEach items="${lista}" var="locacao">
					<tr>
						<td><c:out value="${locacao.id}" /></td>
						<td><c:out value="${locacao.aluno.nome}" /></td>
						<td><c:out value="${locacao.funcionario.nome}" /></td>
						<td><c:out value="${locacao.material.titulo}" /></td>
						<td><c:out value="${locacao.dataDeLocacao}" /></td>
						<td><c:out value="${locacao.dataDeDevolucao}" /></td>
						<td><c:out value="${locacao.reserva}" /></td>
					</tr>
				</c:forEach>
			</table>
		<!-- </form> -->
	</div>


	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>