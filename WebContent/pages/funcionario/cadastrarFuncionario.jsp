<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastrar Aluno/Funcion�rio</title>
<script src="/ProgramacaoWebProjetoP2_JSP/js/validacoes.js"></script>
<link rel="stylesheet"
	href="/ProgramacaoWebProjetoP2_JSP/css/cadastro.css" type="text/css">
</head>
<body>

	<%@include file="/topo.jsp"%>

	<br />
	<form name="form1" style="font-family: monospace; font-size: 20px;" action="../../cadastrarFuncionario" method="post">

		<div>
			<center>
				<h1>Cadastrar Funcion�rio</h1>
			</center>
		</div>

		<table align="center">
			<tr>
				<td><label>Nome: </label></td>
				<td><input type="text" name="nome" /></td>
			</tr>

			<tr>
				<td><label>CPF: </label></td>
				<td><input type="text" name="cpf"
					onBlur="ValidarCPF(form1.cpf);" onKeyPress="MascaraCPF(form1.cpf);"
					maxlength="14" /></td>
			</tr>
			<tr>
				<td><label>T�tulo do mestrado: </label></td>
				<td><input type="text" name="titulodomestrado"/></td>
			</tr>
			<tr>
				<td><label>Ano de conclus�o do mestrado: </label></td>
				<td><input type="number" name="anoconclusaomestrado"/></td>
			</tr>
			
			<tr>
				<td><label>Data de Nascimento: </label></td>
				<td><input type="date" name="dataNascimento" /></td>
			</tr>
			<tr>
				<td><label>Telefone: </label></td>
				<td><input type="text" name="telefone"
					onKeyPress="MascaraTelefone(form1.telefone);" maxlength="14"
					onBlur="ValidaTelefone(form1.tel);" /></td>
			</tr>
		</table>

		<br />

		<div align="center">
			<input type="submit"
				style="width: 200px; height: 30px; font-family: Century Gothic;" />
		</div>
	
	</form>


	<script type="text/javascript"
		src="/ProgramacaoWebProjetoP2_JSP/js/cadastro.js"></script>

	<%@include file="/rodape.jsp"%>
</body>
</html>