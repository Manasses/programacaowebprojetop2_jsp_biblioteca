package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import controller.Utils;
import model.Material;


public class DAOMaterial{

	private Connection con;//objeto connection que ser� usado nos m�todos abaixo
	Utils utils = new Utils();
	/*
	 * Construtor que recebe como parametro uma conexao com o banco de dado. 
	 */
	public DAOMaterial(Connection con){
		this.con = con;
	}

	public void cadastrar(Material material) throws Exception {
		PreparedStatement p =
				con.prepareStatement("insert into material (titulo, autor, editora, dataCadastro, tipoMaterial, qntDisponivel, qntOcupada) values (?,?,?,?,?,?,?)");
		p.setString(1, material.getTitulo());
		p.setString(2, material.getAutor());
		p.setString(3, material.getEditora());

		//p.setDate(4, material.getDataCadastro());
		java.util.Date dataUtil = material.getDataCadastro();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		p.setDate(4, dataSql);

		p.setString(5, material.getTipoMaterial());
		p.setInt(6, material.getQntDisponivel());
		p.setInt(7, material.getQntOcupada());
		p.executeUpdate();
		p.close();
	}

	public void deletar(Material material) throws Exception {
		PreparedStatement p = con.prepareStatement("delete from material where id = ?");
		p.setInt(1, material.getId());
		p.executeUpdate();
		p.close();
	}

	public void update(Material material) throws Exception {
		PreparedStatement p = 
				con.prepareStatement("update material set titulo = ?, autor = ?, editora = ?, dataCadastro = ?, tipoMaterial = ?, qntDisponivel = ?, qntOcupada = ? where id = " + material.getId());

		p.setString(1, material.getTitulo());
		p.setString(2, material.getAutor());
		p.setString(3, material.getEditora());

		//p.setDate(4, material.getDataCadastro());
		java.util.Date dataUtil = material.getDataCadastro();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		p.setDate(4, dataSql);

		p.setString(5, material.getTipoMaterial());
		p.setInt(6, material.getQntDisponivel());
		p.setInt(7, material.getQntOcupada());
		p.executeUpdate();
		p.close();
	}

	public List<Material> listarTodos() throws Exception{
		List<Material> materiais = new ArrayList<Material>();
		PreparedStatement p = con.prepareStatement("select * from material");
		ResultSet rs = p.executeQuery();
		while(rs.next()){
			Material material = new Material();
			material.setId(rs.getInt("id"));
			material.setTitulo(rs.getString("titulo"));
			material.setAutor(rs.getString("autor"));
			material.setEditora(rs.getString("editora"));
			material.setDataCadastro(rs.getDate("dataCadastro"));
			material.setTipoMaterial(rs.getString("tipoMaterial"));
			material.setQntDisponivel(rs.getInt("qntDisponivel"));
			material.setQntOcupada(rs.getInt("qntOcupada"));
			materiais.add(material);
		}
		rs.close();
		p.close();
		return materiais;
	}

	public Material buscarPorId(int id) throws Exception{
		List<Material> materiais = new ArrayList<Material>();
		PreparedStatement p = con.prepareStatement("select * from material where id = " + id);
		ResultSet rs = p.executeQuery();
		while(rs.next()){
			Material material = new Material();
			material.setId(rs.getInt("id"));
			material.setTitulo(rs.getString("titulo"));
			material.setAutor(rs.getString("autor"));
			material.setEditora(rs.getString("editora"));
			material.setDataCadastro(rs.getDate("dataCadastro"));
			material.setTipoMaterial(rs.getString("tipoMaterial"));
			material.setQntDisponivel(rs.getInt("qntDisponivel"));
			material.setQntOcupada(rs.getInt("qntOcupada"));
			materiais.add(material);
		}
		rs.close();
		p.close();

		if(materiais.isEmpty()){
			return new Material();
		}
		else{
			return materiais.get(0);
		}
	}
	
	public Material buscarPorCpf(String cpf) throws Exception{
		List<Material> materiais = new ArrayList<Material>();
		PreparedStatement p = con.prepareStatement("select * from material where id = " + cpf);
		ResultSet rs = p.executeQuery();
		while(rs.next()){
			Material material = new Material();
			material.setId(rs.getInt("id"));
			material.setTitulo(rs.getString("titulo"));
			material.setAutor(rs.getString("autor"));
			material.setEditora(rs.getString("editora"));
			material.setDataCadastro(rs.getDate("dataCadastro"));
			material.setTipoMaterial(rs.getString("tipoMaterial"));
			material.setQntDisponivel(rs.getInt("qntDisponivel"));
			material.setQntOcupada(rs.getInt("qntOcupada"));
			materiais.add(material);
		}
		rs.close();
		p.close();

		if(materiais.isEmpty()){
			return new Material();
		}
		else{
			return materiais.get(0);
		}
	}
}