package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Funcionario extends BaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String cpf;
	private String tituloDoMestrado;
	private int anoConclusaoMestrado;
	private Date dataDeNascimento;
	private String telefone;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTituloDoMestrado() {
		return tituloDoMestrado;
	}
	public void setTituloDoMestrado(String tituloDoMestrado) {
		this.tituloDoMestrado = tituloDoMestrado;
	}
	public int getAnoConclusaoMestrado() {
		return anoConclusaoMestrado;
	}
	public void setAnoConclusaoMestrado(int anoConclusaoMestrado) {
		this.anoConclusaoMestrado = anoConclusaoMestrado;
	}
	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}
	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
