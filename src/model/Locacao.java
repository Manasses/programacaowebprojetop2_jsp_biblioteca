package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Locacao extends BaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	private Aluno aluno;
	@OneToOne
	private Funcionario funcionario;
	@OneToOne
	private Material material;
	private Date dataDeLocacao;
	private Date dataDeDevolucao;
	private Boolean reserva;
	
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Date getDataDeLocacao() {
		return dataDeLocacao;
	}
	public void setDataDeLocacao(Date dataDeLocacao) {
		this.dataDeLocacao = dataDeLocacao;
	}
	public Date getDataDeDevolucao() {
		return dataDeDevolucao;
	}
	public void setDataDeDevolucao(Date dataDeDevolucao) {
		this.dataDeDevolucao = dataDeDevolucao;
	}
	public Boolean getReserva() {
		return reserva;
	}
	public void setReserva(Boolean reserva) {
		this.reserva = reserva;
	}
	
}
