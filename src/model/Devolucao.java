package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Devolucao extends BaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	private Locacao locacao;
	private Date dataDevolucao;
	private boolean atrasoDevolucao;
	private int qntDiasDeAtraso;
	private double valorDiarioDaMulta;
	private double valorTotalDaMulta;
	private boolean pagamentoRealizado;
	private boolean pagamentoNecessario;
	
	public Locacao getLocacao() {
		return locacao;
	}
	public void setLocacao(Locacao locacao) {
		this.locacao = locacao;
	}
	public Date getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	public boolean isAtrasoDevolucao() {
		return atrasoDevolucao;
	}
	public void setAtrasoDevolucao(boolean atrasoDevolucao) {
		this.atrasoDevolucao = atrasoDevolucao;
	}
	public int getQntDiasDeAtraso() {
		return qntDiasDeAtraso;
	}
	public void setQntDiasDeAtraso(int qntDiasDeAtraso) {
		this.qntDiasDeAtraso = qntDiasDeAtraso;
	}
	public double getValorDiarioDaMulta() {
		return valorDiarioDaMulta;
	}
	public void setValorDiarioDaMulta(double valorDiarioDaMulta) {
		this.valorDiarioDaMulta = valorDiarioDaMulta;
	}
	public double getValorTotalDaMulta() {
		return valorTotalDaMulta;
	}
	public void setValorTotalDaMulta(double valorTotalDaMulta) {
		this.valorTotalDaMulta = valorTotalDaMulta;
	}
	public boolean isPagamentoRealizado() {
		return pagamentoRealizado;
	}
	public void setPagamentoRealizado(boolean pagamentoRealizado) {
		this.pagamentoRealizado = pagamentoRealizado;
	}
	public boolean isPagamentoNecessario() {
		return pagamentoNecessario;
	}
	public void setPagamentoNecessario(boolean pagamentoNecessario) {
		this.pagamentoNecessario = pagamentoNecessario;
	}
	
}
