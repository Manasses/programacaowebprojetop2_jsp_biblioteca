package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOMaterialHibernate;
import model.Material;

/**
 * Servlet implementation class cadastrarMaterial
 */
@WebServlet("/excluirMaterial")
public class ExcluirMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOMaterialHibernate daoMaterial = new DAOMaterialHibernate();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExcluirMaterial() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Entrou no m�todo excluir material");

		int id = Integer.parseInt(request.getParameter("id"));
		Material material = daoMaterial.consultarPorId(id);
		daoMaterial.remover(material);

		request.setAttribute("listaDeMateriais", daoMaterial.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/material/listarMateriais.jsp");
		requestDispatcher.forward(request, response);
	}


}
