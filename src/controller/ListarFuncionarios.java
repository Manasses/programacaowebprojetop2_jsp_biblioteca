package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFuncionario;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/listarFuncionarios")
public class ListarFuncionarios extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOFuncionario daoFuncionario = new DAOFuncionario();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListarFuncionarios() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		daoFuncionario = new DAOFuncionario();
		request.setAttribute("listaDeFuncionarios", daoFuncionario.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/funcionario/listarFuncionarios.jsp");
		requestDispatcher.forward(request, response);
	}

}
