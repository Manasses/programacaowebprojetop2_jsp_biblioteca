package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import dao.DAOLocacao;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/listarLocacoes")
public class ListarLocacoes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOLocacao daoLocacao = new DAOLocacao();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListarLocacoes() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		daoLocacao = new DAOLocacao();
		request.setAttribute("listaDeLocacoes", daoLocacao.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/locar_material/listarLocacoes.jsp");
		requestDispatcher.forward(request, response);
	}

}
