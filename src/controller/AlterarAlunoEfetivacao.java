package controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import model.Aluno;

/**
 * Servlet implementation class cadastrarAluno
 */
@WebServlet("/alterarAlunoEfetivacao")
public class AlterarAlunoEfetivacao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Aluno aluno = new Aluno();
	private DAOAluno daoAluno = new DAOAluno();
	private Utils utils = new Utils();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarAlunoEfetivacao() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nome = request.getParameter("nome");
		String nomeDaMae = request.getParameter("nomedamae");
		String cpf = request.getParameter("cpf");
		String telefone = request.getParameter("telefone");
		String dataNascimento = request.getParameter("dataNascimento");
		Date dataNascimentoConverted = null;
		
		try {

			dataNascimentoConverted = utils.formataDataParaBanco(dataNascimento, dataNascimentoConverted);

			if (nome != null) {
				aluno = new Aluno();
				aluno.setId(id);
				aluno.setNome(nome);
				aluno.setNomeDaMae(nomeDaMae);
				aluno.setCpf(cpf);
				aluno.setDataDeNascimento(dataNascimentoConverted);
				aluno.setTelefone(telefone);

				daoAluno.alterar(aluno);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("listaDeAlunos", daoAluno.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/aluno/listarAlunos.jsp");
		requestDispatcher.forward(request, response);
	}
	
	
}
