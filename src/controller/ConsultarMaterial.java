package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOMaterialHibernate;
import model.Material;

/**
 * Servlet implementation class ConsultarMaterial
 */
@WebServlet("/ConsultarMaterial")
public class ConsultarMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOMaterialHibernate daoMaterial = new DAOMaterialHibernate();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConsultarMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Entrou no servlet de consultar material");

		daoMaterial = new DAOMaterialHibernate();

		String idMaterial = request.getParameter("idMaterial");
		System.out.println("idMaterial: " + idMaterial);
		int idMaterialConv = Integer.parseInt(idMaterial);
		Material material = daoMaterial.consultarPorId(idMaterialConv);
		String nomeMaterial = material.getTitulo();
		
		request.setAttribute("idMaterialLocacao", idMaterial);
		
		response.setContentType("text/plain");  
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(nomeMaterial);

	}

}
