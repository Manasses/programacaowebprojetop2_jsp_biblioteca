package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAODevolucao;
import dao.DAOLocacao;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/listarDevolucoes")
public class ListarDevolucoes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAODevolucao daoDevolucao = new DAODevolucao();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListarDevolucoes() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		daoDevolucao = new DAODevolucao();
		request.setAttribute("listaDeDevolucoes", daoDevolucao.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/devolver_material/listarDevolucoes.jsp");
		requestDispatcher.forward(request, response);
	}

}
