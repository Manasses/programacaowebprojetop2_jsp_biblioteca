package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFuncionario;
import model.Funcionario;

/**
 * Servlet implementation class cadastrarFuncionario
 */
@WebServlet("/alterarFuncionario")
public class AlterarFuncionario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOFuncionario daoFuncionario = new DAOFuncionario();
	private Utils utils = new Utils();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarFuncionario() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		int id = Integer.parseInt(request.getParameter("id"));
		Funcionario funcionario = daoFuncionario.consultarPorId(id);
		Date dataNascimento = funcionario.getDataDeNascimento();
		String dataNascimentoConverted = new String();
		
		try {
			dataNascimentoConverted = utils.formataDataParaView(dataNascimento);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		request.setAttribute("funcionario", funcionario);
		request.setAttribute("dataNascimento", dataNascimentoConverted);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/funcionario/alterarFuncionario.jsp");
		requestDispatcher.forward(request, response);
	}


}
