package controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	public Date formataDataParaBanco(String dataNascimento, Date dataNascimentoConverted) throws ParseException {

		if (dataNascimento != null) {
			dataNascimento = dataNascimento.replace('-', '/');

			DateFormat formatUS = new SimpleDateFormat("yyyy/MM/dd");
			dataNascimentoConverted = (Date) formatUS.parse(dataNascimento);

			DateFormat formatBR = new SimpleDateFormat("dd/MM/yyyy");
			String dateFormated = formatBR.format(dataNascimentoConverted);

			dataNascimentoConverted = (Date) formatBR.parse(dateFormated);

			System.out.println("dataNascimento: " + dataNascimento);
			System.out.println("dataNascimentoConverted: " + dataNascimentoConverted);

		}

		return dataNascimentoConverted;

	}
	
	public String formataDataParaView(Date dataNascimento) throws ParseException {

		String dataNascimentoConverted = new String();
		
		if (dataNascimento != null) {

			DateFormat formatUS = new SimpleDateFormat("yyyy-MM-dd");
			dataNascimentoConverted = (String) formatUS.format(dataNascimento);

			System.out.println("dataNascimento: " + dataNascimento);
			System.out.println("dataNascimentoConverted: " + dataNascimentoConverted);

		}

		return dataNascimentoConverted;

	}

}
