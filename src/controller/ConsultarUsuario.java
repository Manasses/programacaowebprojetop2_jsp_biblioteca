package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import dao.DAOFuncionario;
import model.Aluno;
import model.Funcionario;

/**
 * Servlet implementation class ConsultarMaterial
 */
@WebServlet("/ConsultarUsuario")
public class ConsultarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOAluno daoAluno = new DAOAluno();
	private DAOFuncionario daoFuncionario = new DAOFuncionario();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConsultarUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Entrou no servlet de consultar usu�rio");

		daoAluno = new DAOAluno();
		daoFuncionario = new DAOFuncionario();

		String cpfUsuario = request.getParameter("cpfUsuario");
		System.out.println("cpfUsuario: " + cpfUsuario);
		
		// Implementar m�todos de consulta por cpf e descomentar c�digo abaixo
		
		/*Aluno aluno = daoAluno.consultarAlunoCpf(cpfUsuario);
		String nomeUsuario = aluno.getNome();
		
		if(aluno.getNome() == null){
			Funcionario funcionario = daoFuncionario.consultarFuncionarioCpf(cpfUsuario);
			nomeUsuario = funcionario.getNome();
		}*/
		
		Aluno aluno = daoAluno.consultarPorId(1);
		Funcionario funcionario = new Funcionario();
		funcionario.setId(0);
		String nomeUsuario = "Manass�s Almeida";
		
		
		request.setAttribute("idAlunoLocacao", aluno.getId());
		request.setAttribute("idFuncionarioLocacao", funcionario.getId());
		
		response.setContentType("text/plain");  
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(nomeUsuario);

	}

}
