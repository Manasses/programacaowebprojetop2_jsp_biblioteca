package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import model.Aluno;

/**
 * Servlet implementation class cadastrarAluno
 */
@WebServlet("/alterarAluno")
public class AlterarAluno extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOAluno daoAluno = new DAOAluno();
	private Utils utils = new Utils();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarAluno() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		int id = Integer.parseInt(request.getParameter("id"));
		Aluno aluno = daoAluno.consultarPorId(id);
		Date dataNascimento = aluno.getDataDeNascimento();
		String dataNascimentoConverted = new String();
		
		try {
			dataNascimentoConverted = utils.formataDataParaView(dataNascimento);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		request.setAttribute("aluno", aluno);
		request.setAttribute("dataNascimento", dataNascimentoConverted);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/aluno/alterarAluno.jsp");
		requestDispatcher.forward(request, response);
	}


}
