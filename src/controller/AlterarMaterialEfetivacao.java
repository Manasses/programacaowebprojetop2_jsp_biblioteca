package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Conexao;
import dao.DAOMaterial;
import dao.DAOMaterialHibernate;
import model.Material;

/**
 * Servlet implementation class cadastrarMaterial
 */
@WebServlet("/alterarMaterialEfetivacao")
public class AlterarMaterialEfetivacao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Material material = new Material();
	//private DAOMaterialHibernate daoMat = new DAOMaterialHibernate();
	DAOMaterial daoMat = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarMaterialEfetivacao() {
		super();
		try {
			daoMat = new DAOMaterial(Conexao.fabricar());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		String titulo = request.getParameter("tituloDoMaterial");
		String autor = request.getParameter("nomeDoAutor");
		String editora = request.getParameter("nomeDaEditora");
		String tipoMaterial = request.getParameter("tipoDeMaterial");
		int qntDisponivel = Integer.parseInt(request.getParameter("qntDeItens"));
		int qntOcupada = 0;

		try {

			if (id != 0) {

				material = new Material();
				material = daoMat.buscarPorId(id);

				material.setTitulo(titulo);
				material.setAutor(autor);
				material.setEditora(editora);
				material.setTipoMaterial(tipoMaterial);
				material.setQntDisponivel(qntDisponivel);
				material.setQntOcupada(qntOcupada);

				daoMat.update(material);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			request.setAttribute("listaDeMateriais", daoMat.listarTodos());
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/material/listarMateriais.jsp");
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
