package controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFuncionario;
import model.Funcionario;

/**
 * Servlet implementation class cadastrarFuncionario
 */
@WebServlet("/alterarFuncionarioEfetivacao")
public class AlterarFuncionarioEfetivacao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Funcionario funcionario = new Funcionario();
	private DAOFuncionario daoFuncionario = new DAOFuncionario();
	private Utils utils = new Utils();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarFuncionarioEfetivacao() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nome = request.getParameter("nome");
		String cpf = request.getParameter("cpf");
		String tituloDoMestrado = request.getParameter("titulodomestrado");
		int anoConclusaoMestrado = Integer.parseInt(request.getParameter("anoconclusaomestrado"));
		String telefone = request.getParameter("telefone");
		String dataNascimento = request.getParameter("dataNascimento");
		Date dataNascimentoConverted = null;
		
		try {

			dataNascimentoConverted = utils.formataDataParaBanco(dataNascimento, dataNascimentoConverted);

			if (nome != null) {
				funcionario = new Funcionario();
				funcionario.setId(id);
				funcionario.setNome(nome);
				funcionario.setCpf(cpf);
				funcionario.setTituloDoMestrado(tituloDoMestrado);
				funcionario.setAnoConclusaoMestrado(anoConclusaoMestrado);
				funcionario.setDataDeNascimento(dataNascimentoConverted);
				funcionario.setTelefone(telefone);

				daoFuncionario.alterar(funcionario);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("listaDeFuncionarios", daoFuncionario.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/funcionario/listarFuncionarios.jsp");
		requestDispatcher.forward(request, response);
	}
	
	
}
