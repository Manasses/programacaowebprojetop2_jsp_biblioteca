package controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.Days;

import dao.DAODevolucao;
import dao.DAOLocacao;
import model.Devolucao;
import model.Locacao;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/efetuarDevolucao")
public class EfetuarDevolucao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOLocacao daoLocacao = new DAOLocacao();
	private DAODevolucao daoDevolucao = new DAODevolucao();
	private final double valor_diario_multa = 5.0;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EfetuarDevolucao() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("Entrou no m�todo efetuar devolu��o");

		Devolucao devolucao = new Devolucao();
		String codigoLocacao = request.getParameter("codigoLocacao");
		int codigoLocacaoConv = Integer.parseInt(codigoLocacao);

		Locacao locacao = daoLocacao.consultarPorId(codigoLocacaoConv);

		if(locacao.getId() != 0){
			devolucao.setLocacao(locacao);
			devolucao.setDataDevolucao(new Date());
			devolucao.setAtrasoDevolucao(atrasoDevolucao(locacao));
			devolucao.setQntDiasDeAtraso(qntDiasDeAtraso(locacao));
			devolucao.setValorDiarioDaMulta(valor_diario_multa);
			devolucao.setValorTotalDaMulta(valor_diario_multa * qntDiasDeAtraso(locacao));
			devolucao.setPagamentoRealizado(false);
			devolucao.setPagamentoNecessario(atrasoDevolucao(locacao));

			daoDevolucao.inserir(devolucao);
		}

		daoDevolucao = new DAODevolucao();
		request.setAttribute("listaDeDevolucoes", daoDevolucao.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/devolver_material/listarDevolucoes.jsp");
		requestDispatcher.forward(request, response);	
	}

	public boolean atrasoDevolucao(Locacao locacao){
		if(locacao.getDataDeDevolucao().before(new Date())){
			return true;
		}
		else{
			return false;
		}
	}

	public int qntDiasDeAtraso(Locacao locacao){
		if (atrasoDevolucao(locacao) == false){
			return 0;
		}
		else{
			DateTime dataInicial = new DateTime(locacao.getDataDeDevolucao().getYear(),
					locacao.getDataDeDevolucao().getMonth(), locacao.getDataDeDevolucao().getDate(), 0, 0);
			DateTime dataFinal = new DateTime(new Date().getYear(), new Date().getMonth(), new Date().getDate(), 0, 0);

			int diasAtraso = Days.daysBetween(dataInicial, dataFinal).getDays();

			return diasAtraso;
		}
	}

}
