package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/listarAlunos")
public class ListarAlunos extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOAluno daoAluno = new DAOAluno();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListarAlunos() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		daoAluno = new DAOAluno();
		request.setAttribute("listaDeAlunos", daoAluno.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/aluno/listarAlunos.jsp");
		requestDispatcher.forward(request, response);
	}

}
