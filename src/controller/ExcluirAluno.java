package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import model.Aluno;

/**
 * Servlet implementation class cadastrarAluno
 */
@WebServlet("/excluirAluno")
public class ExcluirAluno extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOAluno daoAluno = new DAOAluno();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExcluirAluno() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Entrou no m�todo excluir aluno");

		int id = Integer.parseInt(request.getParameter("id"));
		Aluno aluno = daoAluno.consultarPorId(id);
		daoAluno.remover(aluno);

		request.setAttribute("listaDeAlunos", daoAluno.consultarTodos());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/aluno/listarAlunos.jsp");
		requestDispatcher.forward(request, response);
	}


}
