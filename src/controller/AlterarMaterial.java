package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOMaterialHibernate;
import model.Material;

/**
 * Servlet implementation class cadastrarMaterial
 */
@WebServlet("/alterarMaterial")
public class AlterarMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DAOMaterialHibernate daoMaterial = new DAOMaterialHibernate();
	private Utils utils = new Utils();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarMaterial() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		int id = Integer.parseInt(request.getParameter("id"));
		Material material = daoMaterial.consultarPorId(id);
		Date dataCadastro = material.getDataCadastro();
		String dataCadastroConverted = new String();
		
		try {
			dataCadastroConverted = utils.formataDataParaView(dataCadastro);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		request.setAttribute("material", material);
		request.setAttribute("dataCadastro", dataCadastroConverted);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/material/alterarMaterial.jsp");
		requestDispatcher.forward(request, response);
	}


}
