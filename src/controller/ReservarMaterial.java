package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOAluno;
import dao.DAOFuncionario;
import dao.DAOLocacao;
import dao.DAOMaterialHibernate;
import model.Aluno;
import model.Funcionario;
import model.Locacao;
import model.Material;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/reservarMaterial")
public class ReservarMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Utils utils = new Utils();
	private DAOLocacao daoLocacao = new DAOLocacao();
	private DAOAluno daoAluno = new DAOAluno();
	private DAOFuncionario daoFuncionario = new DAOFuncionario();
	private DAOMaterialHibernate daoMaterial = new DAOMaterialHibernate();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReservarMaterial() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("Entrou no m�todo reservar");

		Locacao locacao = new Locacao();
		Aluno aluno = new Aluno();
		Funcionario funcionario = new Funcionario();
		Material material = new Material();
		Date dataParaReservarConv = null;
		
		try {
			String dataParaReservar = request.getParameter("dataParaReservar");
			dataParaReservarConv = utils.formataDataParaBanco(dataParaReservar, dataParaReservarConv);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		String idMaterial = request.getParameter("idMaterial");
		int idMaterialConv = Integer.parseInt(idMaterial);
		String cpfUsuario = request.getParameter("cpf");

		System.out.println("idMaterialLocacao: " + idMaterial);
		System.out.println("cpfUsuario: " + cpfUsuario);


		material = daoMaterial.consultarPorId(1);
		aluno = daoAluno.consultarPorId(1); // Consultar por cpf
		funcionario = null;
		if(aluno == null){
			funcionario = daoFuncionario.consultarPorId(1); // Consultar por cpf
			aluno = null;
		}

		if(aluno == null && funcionario == null){
			request.setAttribute("mensagem", "Usu�rio inexistente. Verifique o cpf digitado e tente novamente.");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/locar_material/locarMaterial.jsp");
			requestDispatcher.forward(request, response);
		}
		else if(material == null){
			request.setAttribute("mensagem", "Material inexistente. Verifique o c�digo digitado e tente novamente.");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/locar_material/locarMaterial.jsp");
			requestDispatcher.forward(request, response);
		}
		else {

			locacao.setMaterial(material);
			locacao.setAluno(aluno);
			locacao.setFuncionario(funcionario);
			locacao.setDataDeLocacao(dataParaReservarConv);
			Date dataDev = new Date();
			dataDev.setDate(new Date().getDate() + 7);
			locacao.setDataDeDevolucao(dataDev);
			locacao.setReserva(true);
			daoLocacao.inserir(locacao);

			request.setAttribute("mensagem", "Loca��o realizada com sucesso.");
			request.setAttribute("listaDeLocacoes", daoLocacao.consultarTodos());
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/locar_material/listarLocacoes.jsp");
			requestDispatcher.forward(request, response);

		}

		/*request.setAttribute("material", material);
		request.setAttribute("aluno", aluno);
		request.setAttribute("funcionario", funcionario);*/
	}

}
