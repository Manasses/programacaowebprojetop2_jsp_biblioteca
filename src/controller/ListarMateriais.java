package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Conexao;
import dao.DAOMaterial;
import dao.DAOMaterialHibernate;

/**
 * Servlet implementation class cadastrarProfessor
 */
@WebServlet("/listarMateriais")
public class ListarMateriais extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//private DAOMaterialHibernate daoMaterial = new DAOMaterialHibernate();
	DAOMaterial daoMat = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListarMateriais() {
		super();
		try {
			daoMat = new DAOMaterial(Conexao.fabricar());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.setAttribute("listaDeMateriais", daoMat.listarTodos());
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/material/listarMateriais.jsp");
			requestDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
